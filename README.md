#php_jzq_api_client  

#一、环境依赖  

##php版本
> php最低版本以composer.json中require.php为准，现为>=5.3.3 

#二、安装方式
##1.使用composer管理工具安装
> 首先：composer.json的require中加入"ebaoquan/jzq_api":">=2.8.3"(版本以composer.json中version为准) 

> 然后执行composer update即可(或 php composer.phar update) 
```
> composer update
```
##2.没有使用composer工具 
> 请联系对接人员，获取完整包 

#三、使用 
```
1.参考example例子目录，修改example/clientInfo.php 的参数 
2.对//TODO 部份信息填值，然后执行example其它例子.php即可 
3.不使用example，首先php头：require_once \__DIR\__ . '/../../vendor/autoload.php';这个autoload.php文件，其它工具使用时请use引用
```