<?php
/**
 * Created by PhpStorm.
 * User: huhu
 * Date: 2019/9/2
 * Time: 10:46
 */
namespace jzq\test\face;
use com\jzq\api\model\face\FaceH5VideoDownloadRequest;
use jzq\test\ClientInfo;
use org\ebq\api\tool\RopUtils;

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../clientInfo.php';

$requestObj=new FaceH5VideoDownloadRequest();
//* 3方订单号,唯一
$requestObj->orderNo="23929190528000200";
$response=RopUtils::doPostByObj($requestObj,ClientInfo::$app_key,ClientInfo::$app_secret,ClientInfo::$services_url);
//以下为返回的一些处理
$responseJson=json_decode($response);
//对结果打印显示
if($responseJson->success){
    echo $requestObj->getMethod()."->处理成功";
    //保存下载的文件
    file_put_contents("/face.mp4",base64_decode($responseJson->faceVideo));
}else{
    echo $requestObj->getMethod()."->处理失败";
}