<?php
/**
 * Created by PhpStorm.
 * User: huhu
 * Date: 2018/9/6
 * Time: 10:46
 */
namespace jzq\test\face;
use com\jzq\api\model\face\FaceH5Request;
use com\jzq\api\model\menu\face\FaceH5StartFromType;
use jzq\test\ClientInfo;
use org\ebq\api\tool\RopUtils;

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../clientInfo.php';

$requestObj=new FaceH5Request();
//第三方用户ID
$requestObj->userId="//TODO";
//* 回调地址
$requestObj->backUrl="//TODO";
//* 3方订单号,唯一
$requestObj->orderNo="//TODO ";
//* 身份证姓名
$requestObj->name="//TODO ";
//* 身份证号
$requestObj->idNo="//TODO ";
//* 验证打开方式:[手机浏览器|APP]
$requestObj->startFrom=FaceH5StartFromType::$BROWSER;
$response=RopUtils::doPostByObj($requestObj,ClientInfo::$app_key,ClientInfo::$app_secret,ClientInfo::$services_url);
//以下为返回的一些处理
$responseJson=json_decode($response);
//对结果打印显示
print_r("response:".$response."</br>");
print_r("format:</br>");
var_dump($responseJson); //null
if($responseJson->success){
    echo $requestObj->getMethod()."->处理成功";
}else{
    echo $requestObj->getMethod()."->处理失败";
}