<?php
/**
 * 人脸识别检测
 * User: huhu
 * DateTime: 2017/11/27 10:07
 */
namespace jzq\test\face;
use com\jzq\api\model\face\FaceVerifyRequest;
use jzq\test\ClientInfo;
use org\ebq\api\model\bean\UploadFile;
use org\ebq\api\tool\RopUtils;

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../clientInfo.php';


//组建请求参数
$requestObj=new FaceVerifyRequest();
//TODO 证件名称*
$requestObj->fullName="//TODO 证件名称";
//TODO 证件号码*
$requestObj->identityCard="//TODO 证件号码";
//TODO 要校验的图片,File方式*
$requestObj->file=new UploadFile("//TODO 要校验的图片路径");

$response=RopUtils::doPostByObj($requestObj,ClientInfo::$app_key,ClientInfo::$app_secret,ClientInfo::$services_url);
//以下为返回的一些处理
$responseJson=json_decode($response);
print_r("response:".$response."</br>");
print_r("format:</br>");
var_dump($responseJson); //null
if($responseJson->success){
    echo $requestObj->getMethod()."->处理成功";
}else{
    echo $requestObj->getMethod()."->处理失败";
}