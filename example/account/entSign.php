<?php
namespace jzq\test\account;
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../clientInfo.php';

use com\jzq\api\model\account\EntSignRequset;
use jzq\test\ClientInfo;
use org\ebq\api\model\bean\UploadFile;
use org\ebq\api\tool\RopUtils;

/**
 * Created by PhpStorm.
 * User: huhu
 * Date: 2018/11/19
 * Time: 10:15
 */
$requestObj=new EntSignRequset();
$filePath="/ent.png";
//* 章内容
$requestObj->signImgFile=new UploadFile($filePath);
//? 归属企业(不传则为本商户,传入时必须为本商户创建的企业)
//$requestObj->email="xxxanxixxx@163.com";
//* 章名称
$requestObj->signName="自定义章001";


//请求
$response=RopUtils::doPostByObj($requestObj,ClientInfo::$app_key,ClientInfo::$app_secret,ClientInfo::$services_url);
//以下为返回的一些处理
$responseJson=json_decode($response);
print_r("response:".$response."</br>");
print_r("format:</br>");
var_dump($responseJson); //null
if($responseJson->success){
    echo $requestObj->getMethod()."->处理成功<br/>";
}else{
    echo $requestObj->getMethod()."->处理失败<br/>";
    echo $responseJson->error->message."<br/>";
}
