<?php
namespace jzq\test\account;
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../clientInfo.php';

use com\jzq\api\model\account\OrganizationCreateRequest;
use com\jzq\api\model\account\OrganizationReapplyRequest;
use com\jzq\api\model\menu\OrganizationType;
use org\ebq\api\tool\RopUtils;
use org\ebq\api\model\bean\UploadFile;

use jzq\test\ClientInfo;
//组建请求参数
$requestObj=new OrganizationReapplyRequest();
$filePath="/tmp/test.jpg";

//* 上一次注册的邮箱或取得的邮箱
$requestObj->emailOrMobile="//TODO 邮箱";

/**以下字段Name,LegalName,LegalIdentityCard,LegalMobile可选部份填写*/
//企业全称
$requestObj->name="//TODO 企业全称";
//法人名称
$requestObj->legalName="//TODO 法人名称";
//法人证件号(现只支持身份证)
$requestObj->legalIdentityCard="//TODO 法人证件号";
//法人手机号
$requestObj->legalMobile="//TODO 法人手机号";

/**如果要修改证件号相关信息必须修改以下6项的部份或全部*/
//证件类型;修改证件号时,必填*
$requestObj->identificationType=OrganizationCreateRequest::$IDENTIFICATION_TYPE_ALLINONE;//0 多证;1 3证合一
//企业类型;修改证件号时,必填*
$requestObj->organizationType=OrganizationType::$ENTERPRISE; //企业或事业单位
//企业营业执照号或3网合一号;修改证件号时,必填*
$requestObj->organizationRegNo="//TODO 企业营业执照号或3网合一号";
//企业营业执照号或3网合一号照片;修改证件号时,必填*
$requestObj->organizationRegImg=new UploadFile($filePath);
//组织机构号码;修改证件号时,当不为3网合一,必填*
//$requestObj->organizationCode="//TODO 组织机构号码";
//组织机构号码照片;修改证件号时,当不为3网合一,必填*
//$requestObj->organizationCodeImg=new UploadFile($filePath);
//税务登记照;修改证件号时,当不为3网合一且不为事业单位,必填*
//$requestObj->taxCertificateImg=new UploadFile($filePath);



/**以下字段如果要修改,选择填写*/
//申请表,当申请表有误时,请填写
$requestObj->signApplication=new UploadFile($filePath);
//自定义签章图标,当签章审核还未审核前可进行修改上传
$requestObj->signImg=new UploadFile($filePath);


//请求
$response=RopUtils::doPostByObj($requestObj,ClientInfo::$app_key,ClientInfo::$app_secret,ClientInfo::$services_url);
//以下为返回的一些处理
$responseJson=json_decode($response);
print_r("response:".$response."</br>");
print_r("format:</br>");
var_dump($responseJson); //null
if($responseJson->success){
    echo $requestObj->getMethod()."->处理成功";
}else{
    echo $requestObj->getMethod()."->处理失败";
}



