<?php
namespace jzq\test\singlecase;
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../clientInfo.php';
/**
 * Created by PhpStorm.
 * User: huhu
 * Date: 2019/1/17
 * Time: 19:42
 */

use com\jzq\api\model\bean\singlecase\Agent;
use com\jzq\api\model\bean\singlecase\Applyer;
use com\jzq\api\model\bean\singlecase\CaseContract;
use com\jzq\api\model\bean\singlecase\Evidence;
use com\jzq\api\model\bean\singlecase\RebitrationApply;
use com\jzq\api\model\bean\singlecase\Respondent;
use com\jzq\api\model\singlecase\GzSingleCaseRequest;
use jzq\test\ClientInfo;
use org\ebq\api\model\bean\UploadFile;
use org\ebq\api\tool\RopUtils;

//组建请求参数
$requestObj=new GzSingleCaseRequest();
//一个例子图片
$filePath="/tmp/test.jpg";
/**申请人*/
$applyer=new Applyer();
$applyer->address="//地址*";
$applyer->email="//邮箱*";
$applyer->contactAddress="//联系地址*";
$applyer->contacts="//联系人*";
$applyer->gender=0;//*
$applyer->name="申请人";//*
$applyer->identityNo="//申请人证件号*";
$applyer->phone="//手机号";
$applyer->nature=0;
$applyer->frontIdentityFile=new UploadFile($filePath);//*
$applyer->reverseIdentityFile=new UploadFile($filePath);//*
       
/**被申请人*/
$respondent = new Respondent();
$respondent->address="//地址*";
$respondent->legalRepresent="//法定代表人*";
$respondent->email="//邮箱*";
$respondent->contactAddress="//联系地址*";
$respondent->contacts="//联系人*";
$respondent->gender=0;//*
$respondent->name="申请人";//*
$respondent->identityNo="//申请人*";
$respondent->phone="//手机号";
$respondent->nature=0;
$respondent->post="//法人职务*";
$respondent->frontIdentityFile=new UploadFile($filePath);//*
$respondent->reverseIdentityFile=new UploadFile($filePath);//*

/**证据材料*/
$evidences = array();
$evidence=new Evidence();
$evidence->evidenceExplain="证据说明1";
$evidence->hasOriginal=true;
$evidence->evidenceFile=new UploadFile($filePath);//*
$evidence->evidenceFileName="证据1";
array_push($evidences, $evidence);
/**仲裁申请书*/
$filePath="/tmp/test.doc";
$rebitrationApply = new RebitrationApply();
$rebitrationApply->applyFile=new UploadFile($filePath);//*
$rebitrationApply->applyFileName="仲裁申请书";
$rebitrationApply->claimContent="请求事项";
$rebitrationApply->reason="事实及理由";
$rebitrationApply->disputFee=0;
/**合同信息*/
$filePath="/tmp/test.pdf";
$caseContracts=array();
$contract= new CaseContract();
$contract->applyNo="APL105250651xxx700000";
$contract->contractFile=new UploadFile($filePath);//*
array_push($caseContracts, $contract);

$agent = new Agent();
$agent->address="湖南";
$agent->agentType=0;
$agent->contactAddress="湖北";
$agent->email="35439xxx4@qq.com";
$agent->frontIdentityFile=new UploadFile($filePath);//*
$agent->identityNo="130xxx199109xxxxx";
$agent->reverseIdentityFile=new UploadFile($filePath);//*
$agent->name="赵6";
$agent->phone="137xxxx3xxxx";
$agent->power=0;
$agent->sex=0;
$agent->powerDetail="1,2";
$agent->wtsFile=new UploadFile($filePath);//*
$agent->powerDetailZh="1";

//fill all params
$requestObj->applyer=$applyer;
$requestObj->respondent=$respondent;
$requestObj->evidences=$evidences;
$requestObj->rebitrationApply=$rebitrationApply;
$requestObj->caseContracts=$caseContracts;
$requestObj->agent=$agent;

//请求
$response=RopUtils::doPostByObj($requestObj,ClientInfo::$app_key,ClientInfo::$app_secret,ClientInfo::$services_url);

//以下为返回的一些处理
$responseJson=json_decode($response);
print_r("response:".$response."</br>");
print_r("format:</br>");
var_dump($responseJson); //null
if($responseJson->success){
    echo $requestObj->getMethod()."->处理成功";
}else{
    echo $requestObj->getMethod()."->处理失败";
}