<?php
/**
 * Created by PhpStorm.
 * User: yfx
 * Date: 2018/11/16
 * Time: 7:13
 */
namespace jzq\test\sign;
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../clientInfo.php';

use com\jzq\api\model\bean\Signatory;
use com\jzq\api\model\menu\IdentityType;
use com\jzq\api\model\sign\SignAddSignatoriesRequest;
use org\ebq\api\model\bean\UploadFile;
use org\ebq\api\tool\RopUtils;
use jzq\test\ClientInfo;
//组建请求参数
$requestObj=new SignAddSignatoriesRequest();
//* 签约编号
$requestObj->applyNo="//TODO APL8744xxxxxxxxxxxxxxx";

//* 签约方
$signatories=array();
//签约方1
$signatory=new Signatory();
//* 证件类型
$signatory->setSignatoryIdentityType(IdentityType::$IDCARD);
//* 名称或公司名称
$signatory->fullName="xxx";
//* 证件号码、营业执照号、社会信用号
$signatory->identityCard="50024xxxxxxxxxxxxxxxx";
//* 手机号码,为个人时必填,企业可不填
$signatory->mobile='xxxxxxx';
//签约方1的校验等级，如设置将覆盖ApplySignFileRequest->authLevel
//$signatory->authLevel=[
//   AuthLevel::$USEKEY,AuthLevel::$FACE
// ];
//签约方1的验证范围
//$signatory->authLevelRange="1";//验证范围，为string，暂只支持正整数，且小于验证方式数量。
//强制阅读时间
//$signatory->readTime=60;//等待时间
//强制身份认证
//$signatory->forceAuthentication=0;
//$signatory->signLevel=SignLevel::$ESIGNSEAL;
//$signatory->noNeedEvidence=0;
//$signatory->forceEvidence=0;
//当orderFlag1时必须指定签约方的orderNum
//$signatory->orderNum=1;
//签字位置目前支持pdf本身有表单域参考(applySignHtml.php)的pdf文件或使用json串确定固定位置方式
//以下使用json串确定位置。页以0开始，注意
//* 签约方
$signatory->setChapteJson(array(
    array(
        'page'=>0,
        'chaptes'=>array(
            array("offsetX"=>0.12,"offsetY"=>0.23),
            array("offsetX"=>0.45,"offsetY"=>0.67)
        )
    ),
    array(
        'page'=>2,
        'chaptes'=>array(
            array("offsetX"=>0.5,"offsetY"=>0.5)
        )
    )
));
array_push($signatories, $signatory);

$requestObj->signatories=$signatories;
$requestObj->isArchive=0;//0不归档,1归档,默认不归档（设置为1时可以不传签约人信息）
//* 请求
$response=RopUtils::doPostByObj($requestObj,ClientInfo::$app_key,ClientInfo::$app_secret,ClientInfo::$services_url);
//以下为返回的一些处理
$responseJson=json_decode($response);
print_r("response:".$response."</br>");
print_r("format:</br>");
var_dump($responseJson); //null
if($responseJson->success){
    echo $requestObj->getMethod()."->处理成功";
}else{
    echo $requestObj->getMethod()."->处理失败";
}
