<?php
namespace jzq\test\sign;
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../clientInfo.php';

use com\jzq\api\model\bean\Signatory;
use com\jzq\api\model\menu\IdentityType;
use com\jzq\api\model\sign\ApplySignHashRequest;
use org\ebq\api\tool\RopUtils;
use org\ebq\api\tool\ShaUtils;

use jzq\test\ClientInfo;

//组建请求参数
$requestObj=new ApplySignHashRequest();
$requestObj->hashValue=ShaUtils::getFileSha512("/tmp/test.pdf");

$requestObj->contractName="//TODO *合同名称";
//* 合同名称
//签合同方|测试时请改为自己的个人信息进行测试（姓名、身份证号、手机号不能部分或全部隐藏）
$signatories=array();
$signatory=new Signatory();
$signatory->setSignatoryIdentityType(IdentityType::$IDCARD);//TODO 证件类型
$signatory->fullName="//TODO 姓名或公司名";
$signatory->identityCard="//TODO 证件号";
$signatory->mobile='//TODO 手机号码';

array_push($signatories, $signatory);

$requestObj->signatories=$signatories;
//请求
$response=RopUtils::doPostByObj($requestObj,ClientInfo::$app_key,ClientInfo::$app_secret,ClientInfo::$services_url);
//以下为返回的一些处理
$responseJson=json_decode($response);
print_r("response:".$response."</br>");
print_r("format:</br>");
var_dump($responseJson); //null
if($responseJson->success){
    echo $requestObj->getMethod()."->处理成功";
}else{
    echo $requestObj->getMethod()."->处理失败";
}