<?php
namespace jzq\test\http;
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../clientInfo.php';
use org\ebq\api\tool\HttpSignUtils;
use jzq\test\ClientInfo;
use org\ebq\api\tool\RopUtils;

/**
 * 此为老的接口回调，暂只适用于签约回调业务
 * 签字原理，取到所有request的body的请求(不包括url的请求参数)参数，组成一个有key,val的数据(也叫map或数据字典)requestParams
 * requestParams去掉key为sign,timestamp的记录
 * 然后对requestParams的key取正序排序记录。
 * 然后按 requestParams->key.requestParams->val.appkey.appsecret 得到字符串str取其sha1即为签名信息
 *
 *
 * 1.签约回调参数说明(除sign,timestamp外的参数)
 * applyNo string 签约编号
 * identityType integer 证件类型
 * fullName string 名称
 * identityCard string 证件号
 * optTime long 操作时间
 * signStatus integer 签约状态1已签2拒签3保全
 *
 */
/**
 * 断言，php默认的aaser不阻塞程序执行
 * @param $assertion
 * @param $description
 * @throws \Exception
 */
function assert_error($assertion, $description){
    if(!$assertion){
        throw new \Exception(is_null($description)?"assert error":$description);
    }
}
$bodyParams=array();
$sign=null;
$timestamp=null;
//$_POST 只处理post的请求body
foreach ($_POST as $key => $value) {
    if($key=='sign'){
        $sign=$value;
        continue;
    }
    if($key=='timestamp'){
        $timestamp=$value;
        continue;
    }
    $bodyParams[$key]=$value;
}
$result=array(
			"resultCode"=>"",
			"msg"=>"",
			"success"=>true,
	);
try{
    //assert_error(!is_null($sign),"签名不能为空");
    //assert_error(!is_null($timestamp),"时间戳不能为空");
    //签名校验，校验不过会抛出异常
    //HttpSignUtils::checkHttpSign($bodyParams, $timestamp, ClientInfo::$app_key, ClientInfo::$app_secret, $sign);
    //打印post请求的body（最原始的）
    //echo(file_get_contents('php://input')."\r\n");
    //TODO 处理业务
    $fp = fopen("/text.txt",'w');
    fwrite($fp,json_encode($bodyParams));
    fclose($fp);
}catch (\Exception $e){
    $result["resultCode"]="otherError";
    $result["msg"]=$e->getMessage();
    $result["success"]=false;
}
echo RopUtils::json_encode($result);