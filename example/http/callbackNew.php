<?php
namespace jzq\test\http;
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../clientInfo.php';
use org\ebq\api\tool\HttpSignUtils;
use jzq\test\ClientInfo;
use org\ebq\api\tool\RopUtils;

/**
 * 此为新的接口回调
 * 和老接口区别：相关业务信息封装到了data参数上(为json的字符串)，增加method参数和version参数（即现在固定参数为data,sign,timestamp,method,version）,签字原理不变
 * 签字原理，取到所有request的body的请求(不包括url的请求参数)参数，组成一个有key,val的数据(也叫map或数据字典)requestParams
 * requestParams去掉key为sign,timestamp的记录
 * 然后对requestParams的key取正序排序记录。
 * 然后按 requestParams->key.requestParams->val.appkey.appsecret 得到字符串str取其sha1即为签名信息
 * ------------------------
 * 使用场景
 * 1.创建用户记录结果（method:organization.audit.status）
 * emailOrMobile
 * status
 *
 * 2.银行卡3要素(method:bank.three.status)
 * verifyStatus
 * executeStatus
 * orderNo
 * resultMessage
 *
 * 3.ca cert apply(method:serv.certi.status)
 * identityCard 证件号
 * userType 用户类型
 * name 名称
 * status
 */

/**
 * 断言，php默认的aaser不阻塞程序执行
 * @param $assertion
 * @param $description
 * @throws \Exception
 */
function assert_error($assertion, $description){
    if(!$assertion){
        throw new \Exception(is_null($description)?"assert error":$description);
    }
}
$bodyParams=array();
$sign=null;
$timestamp=null;
$method=null;
$version=null;
//$_POST 只处理post的请求body
foreach ($_POST as $key => $value) {
    if($key=='sign'){
        $sign=$value;
        continue;
    }
    if($key=='timestamp'){
        $timestamp=$value;
        continue;
    }
    if($key=='method'){
        $method=$value;
    }
    if($key=='version'){
        $version=$value;
    }
    $bodyParams[$key]=$value;
}
$result=array(
			"resultCode"=>"",
			"msg"=>"",
			"success"=>true,
	);
try{
    assert_error(!is_null($sign),"签名不能为空");
    assert_error(!is_null($timestamp),"时间戳不能为空");
    assert_error(!is_null($method),"method不能为空");
    assert_error(!is_null($version),"version不能为空");
    //签名校验，校验不过会抛出异常
    HttpSignUtils::checkHttpSign($bodyParams, $timestamp, ClientInfo::$app_key, ClientInfo::$app_secret, $sign);
    //打印post请求的body（最原始的）
    //echo(file_get_contents('php://input')."\r\n");
    //TODO 处理业务
}catch (\Exception $e){
    $result["resultCode"]="otherError";
    $result["msg"]=$e->getMessage();
    $result["success"]=false;
}
echo RopUtils::json_encode($result);