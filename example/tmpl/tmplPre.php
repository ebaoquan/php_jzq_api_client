<?php
/**
 * Created by huhu.
 * User: Administrator
 * Date: 2018/9/6
 * Time: 20:44
 */
namespace jzq\test\sign;
use com\jzq\api\model\tmpl\TmplPreRequest;
use jzq\test\ClientInfo;
use org\ebq\api\tool\RopUtils;

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../clientInfo.php';


//组建请求参数
$requestObj=new TmplPreRequest();
//* 模板编号
$requestObj->templateNo="T001";
//* 参数
$requestObj->contractParams=array(
    "NO"=>"SN123456789",
    "projectName"=>"君子签",
    "dept"=>"技术部",
    "name"=>"文松"
);

//请求,当参数超过1000字符时建议开始mutipart post上传
//$requestObj->contentType="multipart/form-data; boundary=".UploadFile::$boundary;
$response=RopUtils::doPostByObj($requestObj,ClientInfo::$app_key,ClientInfo::$app_secret,ClientInfo::$services_url);
//以下为返回的一些处理
$responseJson=json_decode($response);
if($responseJson->success){
    echo $requestObj->getMethod()."->处理成功<br/>\r\n";
    echo $responseJson->html;
}else{
    echo $requestObj->getMethod()."->处理失败";
}