<?php
/**
 * User: huhu
 * DateTime: 2017/8/23 16:15
 */
namespace jzq\test\ocr;
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../clientInfo.php';

use org\ebq\api\model\bean\UploadFile;
use org\ebq\api\tool\RopUtils;
use com\jzq\api\model\ocr\IdentityOCRRequest;
use jzq\test\ClientInfo;

//组建请求参数
$requestObj=new IdentityOCRRequest();
/**
 * 是否压缩图片。如果不压缩图片，可能会影响识别效率。默认0(压缩),1不压缩
 * 如果图片压缩后不能正确的识别银行卡信息，请把这个值设置为1再试。
 */
//$requestObj->isCompress=0;
//待识别的证件
$requestObj->file=new UploadFile("//TODO *待识别的证件图片,尽量小而清淅");

$response=RopUtils::doPostByObj($requestObj,ClientInfo::$app_key,ClientInfo::$app_secret,ClientInfo::$services_url);
//以下为返回的一些处理
$responseJson=json_decode($response);
print_r("response:".$response."</br>");
print_r("format:</br>");
var_dump($responseJson); //null
if($responseJson->success){
    echo $requestObj->getMethod()."->处理成功";
}else{
    echo $requestObj->getMethod()."->处理失败";
}