<?php
namespace jzq_self\test\arbitration;
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../clientInfo.php';

use com\jzq\api\model\arbitration\ArbitrationApplyRequest;
use com\jzq\api\model\bean\arbitration\ArbitrationClaimant;
use com\jzq\api\model\bean\arbitration\ArbitrationEvidence;
use com\jzq\api\model\bean\arbitration\ArbitrationLoanInfo;
use com\jzq\api\model\bean\arbitration\ArbitrationRespondent;
use com\jzq\api\model\bean\arbitration\VoucherRemark;
use com\jzq\api\model\menu\arbitration\ArbitrationIdentityType;
use com\jzq\api\model\menu\BorrowDaysType;
use com\jzq\api\model\menu\arbitration\BusinessTransferType;
use com\jzq\api\model\menu\RepaymentType;
use com\jzq\api\model\menu\GenderType;
use com\jzq\api\model\menu\YesOrNo;
use org\ebq\api\tool\RopUtils;
use org\ebq\api\model\bean\UploadFile;
use jzq_self\test\ClientInfo;

$requestObj=new ArbitrationApplyRequest();
$requestObj->applyNo="//TODO *签约编号";

//申请人信息
$claimant=new ArbitrationClaimant();
/**申请人信息-自然人*/
/**
$claimant->type=ArbitrationIdentityType::$NATURAL_MAN;//申请人类型-个人
$claimant->name="//TODO *原告名称";
$claimant->nation="//TODO *民族";
$claimant->gender=GenderType::$MALE;//性别
$claimant->identityNo="//TODO *证件号";
$claimant->mobileNo="//TODO *手机号";
$claimant->bankCardNo="//TODO *银行卡号";
$claimant->bankCardMobile="//TODO *银行预留手机号码";
$claimant->email="//TODO 邮箱";
$claimant->address="//TODO 地址";
 */
/**申请人信息-企业*/
$claimant->type=ArbitrationIdentityType::$ENTERPRISE;//申请人类型-企业
$claimant->enterpriseName="//TODO *公司全称";
$claimant->businessLicence="//TODO *公司营业执照或社会信用号";
$claimant->name="//TODO *法人姓名";
$claimant->identityNo="//TODO *法人证件号";
$claimant->mobileNo="//TODO *手机号";
$claimant->email="//TODO 邮箱";
$claimant->address="//TODO 地址";
$claimant->legalJob="//TODO 职位";
$requestObj->claimant=$claimant;

//被告信息
$respondent=new ArbitrationRespondent();
/**被告信息-自然人*/
/**
$respondent->type=ArbitrationIdentityType::$NATURAL_MAN;//被告类型-个人
$respondent->name="//TODO *名称";
$respondent->nation="//TODO *民族";
$respondent->gender=GenderType::$MALE;//性别
$respondent->identityNo="//TODO *证件号";
$respondent->mobileNo="//TODO *手机号";
$respondent->bankCardNo="//TODO *银行卡号";
$respondent->bankCardMobile="//TODO *银行预留手机号码";
$respondent->email="//TODO 邮箱";
$respondent->address="//TODO 地址";
 */
/**被告信息-企业*/
$respondent->type=ArbitrationIdentityType::$ENTERPRISE;//被告类型-企业
$respondent->enterpriseName="//TODO *公司全称";
$respondent->businessLicence="//TODO *公司营业执照或社会信用号";
$respondent->name="//TODO *法人姓名";
$respondent->identityNo="//TODO *法人证件号";
$respondent->mobileNo="//TODO *手机号";
$respondent->email="//TODO 邮箱";
$respondent->address="//TODO 地址";
$respondent->legalJob="//TODO 职位";
$requestObj->respondent=$respondent;
//仲载申请业务
$requestObj->businessType=BusinessTransferType::$LOAN;//仲载业务类型
$loanInfo = new ArbitrationLoanInfo();
$loanInfo->contractName='//TODO *合同名称';
$loanInfo->signDate = strtotime("2018-07-01 06:21:32")*1000;//签订日期
$loanInfo->borrowDate  = strtotime("2018-07-01 07:28:35")*1000;//出借日期
$loanInfo->borrowMoney=1000.00;//出借金额-元
$loanInfo->repaymentType=RepaymentType::$DISPOSABLE;//还款方式
$loanInfo->borrowDays=7;//还款限期

$loanInfo->borrowDaysType=BorrowDaysType::$TERMS;//按天
$loanInfo->repayment=1000;//每期还款金额
$loanInfo->monthMoneyRate=1.9;//月利率，borrowDaysType=1，则必填，1.9% 填1.9

$loanInfo->moneyRate=1.6;//借款利率(年百分比)
$loanInfo->interest=100;//应还利息
$loanInfo->overdueMoney=3.2;//逾期利率
$loanInfo->lastestRepaymentDate=strtotime("2018-07-06")*1000;//末次还款日期
$loanInfo->repayMoney=1;//已还金额
$loanInfo->repaymentDate=strtotime("2018-07-08")*1000;//应还款日期

$loanInfo->serviceFee=20;//居间服务费
$loanInfo->paidServiceFee=5;//已还居间服务费

$loanInfo->penalty=88; //违约金
$loanInfo->penaltyRate=1.5;//违约金利率（日利率）， 若penalty 填了，则必填

$loanInfo->fine=8;//罚息
$loanInfo->fineRate=1.8;//罚息利率（日利率）， 若fine 填了，则必填
$loanInfo->pawn=YesOrNo::$YES;//是否有抵押物 1=有，2=否  非必填
$loanInfo->pawnSold=YesOrNo::$YES;
$loanInfo->pawnSoldMoney=500;//变卖金额  若pawn填了，则必填
$loanInfo->layerServiceMoney=300;//律师服务费 非必填
$requestObj->business=$loanInfo;

//证据清单
$evidence=new ArbitrationEvidence();
$evidence->claimantIdentityImg=new UploadFile("/tmp/test.png");//申请人身份证/营业执照扫描件
$evidence->respondentIdentityImg=new UploadFile("/tmp/test.png");//被申请人身份证
$evidence->claimantLegalIdNoImg=new UploadFile("/tmp/test.png");//申请人法人身份证
$evidence->respondentLegalIdNoImg=new UploadFile("/tmp/test.png");//被申请人法人身份证
//转账凭证记录
$transferVoucherList=array(
    new VoucherRemark(new UploadFile("/tmp/test.png"),"备注1"),
    new VoucherRemark(new UploadFile("/tmp/test.png"),"备注2"),
    );
$evidence->transferVoucherList=$transferVoucherList;
//辅助凭证记录
$assistantVoucherList=array(
    new VoucherRemark(new UploadFile("/tmp/test.png"),"备注1"),
    new VoucherRemark(new UploadFile("/tmp/test.png"),"备注2"),
);
$evidence->assistantVoucherList=$assistantVoucherList;
$requestObj->evidence=$evidence;
    //请求
$response=RopUtils::doPostByObj($requestObj,ClientInfo::$app_key,ClientInfo::$app_secret,ClientInfo::$services_url);
//以下为返回的一些处理
$responseJson=json_decode($response);
print_r("response:".$response."</br>");
print_r("format:</br>");
var_dump($responseJson); //null
if($responseJson->success){
    echo $requestObj->getMethod()."->处理成功";
}else{
    echo $requestObj->getMethod()."->处理失败";
}