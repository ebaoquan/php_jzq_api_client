<?php
namespace jzq\test\sms;
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../clientInfo.php';
/**
 * Created by PhpStorm.
 * User: yfx
 * Date: 2018/8/9
 * Time: 16:44
 */

use com\jzq\api\model\sms\SmsCodeSendRequest;
use org\ebq\api\tool\RopUtils;
use jzq\test\ClientInfo;
//组建请求参数
$requestObj=new SmsCodeSendRequest();
//* 短信模版
$requestObj->templateNo="//TODO 申请到的模版NO";
//* 手机号
$requestObj->mobile='//TODO 手机号码';
//验证码有效时长:1分钟-1小时内,单位毫秒
//$requestObj->exipreLen=20*60*1000;
//除验证码的code(验证码)和num(序列号)外存在其它的参数时使用
//$requestObj->params=array(""=>"",""=>"");
//请求
$response=RopUtils::doPostByObj($requestObj,ClientInfo::$app_key,ClientInfo::$app_secret,ClientInfo::$services_url);

//以下为返回的一些处理
$responseJson=json_decode($response);
print_r("response:".$response."</br>");
print_r("format:</br>");
var_dump($responseJson); //null
if($responseJson->success){
    echo $requestObj->getMethod()."->处理成功";
}else{
    echo $requestObj->getMethod()."->处理失败";
}