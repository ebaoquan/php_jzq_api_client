<?php
namespace jzq\test\sms;
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../clientInfo.php';
/**
 * Created by PhpStorm.
 * User: yfx
 * Date: 2018/8/9
 * Time: 16:44
 */
use com\jzq\api\model\sms\SmsCodeVerifyRequest;
use org\ebq\api\tool\RopUtils;
use jzq\test\ClientInfo;
//组建请求参数
$requestObj=new SmsCodeVerifyRequest();
//* 短信发送编号
$requestObj->bizNo="//TODO 发送后的验证码";
//* 手机收到的验证码
$requestObj->code='//TODO 手机获取到的验证码';
//验证成功是否失效短信记录（失效后不能再验证）
//$requestObj->exipreAfterSuccess=true;
//验证是否保全
//$requestObj->presFlag=1;
//请求
$response=RopUtils::doPostByObj($requestObj,ClientInfo::$app_key,ClientInfo::$app_secret,ClientInfo::$services_url);

//以下为返回的一些处理
$responseJson=json_decode($response);
print_r("response:".$response."</br>");
print_r("format:</br>");
var_dump($responseJson); //null
if($responseJson->success){
    echo $requestObj->getMethod()."->处理成功";
}else{
    echo $requestObj->getMethod()."->处理失败";
}