<?php
namespace jzq\test\auth;
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../clientInfo.php';
use com\jzq\api\model\auth\UserValidRequest;
use org\ebq\api\tool\RopUtils;
use jzq\test\ClientInfo;
//二要素认证sample
//组建请求参数
$requestObj=new UserValidRequest();
//* 名称
$requestObj->name="//TODO";
//* 身份证号
$requestObj->identityNo="//TODO";
//请求
$response=RopUtils::doPostByObj($requestObj,ClientInfo::$app_key,ClientInfo::$app_secret,ClientInfo::$services_url);
//以下为返回的一些处理
$responseJson=json_decode($response);
print_r("response:".$response."</br>");
print_r("format:</br>");
var_dump($responseJson); //null
if($responseJson->success){
    echo $requestObj->getMethod()."->处理成功";
}else{
    echo $requestObj->getMethod()."->处理失败";
}