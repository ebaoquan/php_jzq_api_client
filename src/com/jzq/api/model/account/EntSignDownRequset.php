<?php
/**
 * Created by PhpStorm.
 * User: huhu
 * Date: 2018/11/19
 * Time: 10:04
 */

namespace com\jzq\api\model\account;


use org\ebq\api\model\RichServiceRequest;

class EntSignDownRequset extends RichServiceRequest{
    static $v="1.0";
    static $method="ent.sign.down";
    /**
     * 企业用户邮箱
     */
    public $email;

    /**章ID*/
    public $signId;

    function validate(){
        $this->email=parent::trim($this->email);
        if(!(isset($this->signId)&&is_long($this->signId))){
            throw new \RuntimeException("signId must be a long type");
        }
        return true;
    }

    function getIgnoreSign(){
        return parent::getIgnoreSign();
    }
}