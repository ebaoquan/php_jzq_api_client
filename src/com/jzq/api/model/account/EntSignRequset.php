<?php
/**
 * Created by PhpStorm.
 * User: huhu
 * Date: 2018/11/19
 * Time: 10:04
 */

namespace com\jzq\api\model\account;


use org\ebq\api\model\RichServiceRequest;
use RuntimeException;

class EntSignRequset  extends RichServiceRequest{
    static $v="1.0";
    static $method="ent.sign";

    /**
     * 企业签章名字
     */
    public $signName;

    /**
     * 企业用户邮箱
     */
    public $email;

    /**多签章图片*/
    public $signImgFile;

    function validate(){
        $this->signName=parent::trim($this->signName);
        $this->email=parent::trim($this->email);
        if($this->signName==''){
            throw new RuntimeException("signName is null");
        }
        if($this->signImgFile!=null&&!is_a($this->signImgFile, 'org\ebq\api\model\bean\UploadFile')){
            throw new RuntimeException("signImgFile is not a UploadFile value");
        }
        return parent::validate();
    }

    function getIgnoreSign(){
        $ignoreSign=array('signImgFile');
        $parr=parent::getIgnoreSign();
        if(is_array($parr)){
            $ignoreSign=array_merge($ignoreSign,$parr);
        }
        return $ignoreSign;
    }
}