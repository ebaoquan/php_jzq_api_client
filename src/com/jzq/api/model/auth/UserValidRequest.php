<?php
namespace com\jzq\api\model\auth;

use org\ebq\api\model\RichServiceRequest;

/**
 * 二要素认证request
 * Class UserValidRequest
 * @package com\jzq\api\model\auth
 */
class UserValidRequest extends RichServiceRequest{
    static $v="1.0";
    static $method="user.valid";

    /**姓名*/
    public $name;

    /**身份证号*/
    public $identityNo;

    function validate(){
        $this->name=self::trim($this->name);
        $this->identityNo=self::trim($this->identityNo);
        if($this->name==''){
            throw new \RuntimeException("姓名不能为空");
        }
        if($this->identityNo==''){
            throw new \RuntimeException("身份证号不能为空");
        }
        return parent::validate();
    }
}