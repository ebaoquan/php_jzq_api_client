<?php
namespace com\jzq\api\model\menu;
/**
 * @author yfx 2018-08-20
 * 仲裁申请-借贷类型
 */
class RepaymentType{
    /**等额本息*/
    static $DEBX="0";
    /**一次性偿还*/
    static $DISPOSABLE="1";
}