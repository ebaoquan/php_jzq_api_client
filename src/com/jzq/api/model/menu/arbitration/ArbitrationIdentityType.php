<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/20
 * Time: 11:25
 */

namespace com\jzq\api\model\menu\arbitration;

/**
 * @author yfx 2018-08-20
 * 仲裁申请-仲裁参与者身份类型
 */
class ArbitrationIdentityType{
    /**自然人*/
    static $NATURAL_MAN='0';
    /**企业*/
    static $ENTERPRISE='1';
}