<?php
namespace com\jzq\api\model\menu;
/**
 * @author yfx 2018-08-20
 * 仲裁申请-还款期限类型
 */
class BorrowDaysType{
    /**按天*/
    static $DAY="0";
    /**按期*/
    static $TERMS="1";
}