<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/20
 * Time: 11:27
 */

namespace com\jzq\api\model\menu;

/**
 * @author yfx 2018-08-20
 * 性别类型
 */
class GenderType{
    /**女*/
    static $FEMALE='0';
    /**男*/
    static $MALE='1';
}