<?php
namespace com\jzq\api\model\menu;

/**
 * 是否 枚举类。
 * 0空置不用
 * Created by hxj on 2018/7/10.
 */
class YesOrNo{
    /**是*/
    static $YES=1;
    /**否*/
    static $NO=2;
}