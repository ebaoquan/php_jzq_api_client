<?php
/**
 * Created by PhpStorm.
 * User: huhu
 * Date: 2018/9/6
 * Time: 10:40
 */

namespace com\jzq\api\model\menu\face;
/**
 * 人脸启动方式
 * Class FaceH5StartFromType
 * @package com\jzq\api\model\menu\face
 */
class FaceH5StartFromType{
    /**浏览器*/
    public static $BROWSER="1";
    /**APP*/
    public static $APP="1";
}