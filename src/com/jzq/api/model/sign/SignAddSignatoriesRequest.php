<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/16
 * Time: 5:27
 */

namespace com\jzq\api\model\sign;


use org\ebq\api\model\RichServiceRequest;
use org\ebq\api\tool\RopUtils;
use RuntimeException;

class SignAddSignatoriesRequest extends RichServiceRequest{
    static $v="1.0";
    static $method="sign.signatories.add";
    /**
     * 签收方
     * 不超过10个签约人
     */
    public $signatories;

    /*
     * applyNo签约编号
     */
    public $applyNo;

    /**
     * 0，1是否归档 ，添加完成签约时设置 isArchive为1 Signatory 可为空
     */
    public $isArchive;


    function validate(){
        $this->isArchive=self::trim($this->isArchive);
        if($this->applyNo==''){
            throw new RuntimeException("applyNo is null");
        }
        foreach ($this->signatories as $signatory) {
            if($signatory==null||!is_a($signatory,'com\jzq\api\model\bean\Signatory')){
                throw new RuntimeException("signatories.value isn't a Signatory value");
            }
            if(!$signatory->validate()){
                return false;
            }
        }
        //php5.4.0-的参考SignLinkRequest方法
        $this->signatories=RopUtils::json_encode($this->signatories);
        return parent::validate();
    }

}