<?php
namespace com\jzq\api\model\sign;

use org\ebq\api\model\RichServiceRequest;
use RuntimeException;

class CancelSignRequest extends RichServiceRequest{

    static $v="1.0";
    static $method="sign.cancel";

    /**合同编号*/
    public $applyNo;

    function validate(){
        $this->applyNo = static::trim($this->applyNo);
        if($this->applyNo==''){
            throw new RuntimeException("applyNo is null");
        }
        return parent::validate();
    }
}