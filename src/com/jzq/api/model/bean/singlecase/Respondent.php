<?php
/**
 * Created by PhpStorm.
 * User: yfx
 * Date: 2018/8/9
 * Time: 16:30
 */

namespace com\jzq\api\model\bean\singlecase;
use org\ebq\api\model\bean\UploadFile;
use org\ebq\api\tool\RopUtils;
use RuntimeException;

/**
 * 被申请人
 */
class Respondent{
    // 名称
    public $name;

    // 性质(个人、公司)
    public $nature;

    // 手机
    public $phone;

    // 邮箱
    public $email;

    // 证件地址
    public $address;

    // 联系地址
    public $contactAddress;

    // 身份证正面
    public $frontIdentityFile;


    // 身份证反面
    public $reverseIdentityFile;

    // 证件号
    public $identityNo;

    // 性别(0:男、1:女)
    public $gender;

    // 法定代表人
    public $legalRepresent;

    // 法人职务
    public $post;

    /**
     * 联系人
     */
    public $contacts;

    /**
     * 验证数据
    */
    function validate(){
        if(!(isset($this->name)&&is_string($this->name))){
            throw new RuntimeException("被申请人姓名不能为空");
        }
        if(!(isset($this->address)&&is_string($this->address))){
            throw new RuntimeException("被申请人证件地址不能为空");
        }
        $this->nature= self::trim($this->nature);
        $this->gender= self::trim($this->gender);
        if($this->frontIdentityFile==null||!is_a($this->frontIdentityFile, 'org\ebq\api\model\bean\UploadFile')){
            throw new RuntimeException("被申请人身份证正面照文件未输入或不正确");
        }
        if($this->reverseIdentityFile==null||!is_a($this->reverseIdentityFile, 'org\ebq\api\model\bean\UploadFile')){
            throw new RuntimeException("被申请人身份证反面照文件未输入或不正确");
        }
        UploadFile::convertBase64($this->frontIdentityFile);
        UploadFile::convertBase64($this->reverseIdentityFile);
    }

    /**
     * @param null $str
     * @return string
     */
    static function trim($str=null){
        if(is_null($str)){
            if(is_numeric($str)){
                return '0';
            }
            return '';
        }else{
            return trim($str.'');
        }
    }
}