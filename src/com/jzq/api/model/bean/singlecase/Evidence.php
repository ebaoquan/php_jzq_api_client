<?php
/**
 * Created by PhpStorm.
 * User: yfx
 * Date: 2018/8/9
 * Time: 16:30
 */

namespace com\jzq\api\model\bean\singlecase;
use org\ebq\api\model\bean\UploadFile;
use RuntimeException;

/**
 * 证据材料
 */
class Evidence{
    //证据材料名称
    public $evidenceFileName;

    //证据材料路径
    public $evidenceFile;

    //证据说明
    public $evidenceExplain;

    /**
     * 是否有原件
     */
    public $hasOriginal;

    function validate(){
        if($this->evidenceFile==null||!is_a($this->evidenceFile, 'org\ebq\api\model\bean\UploadFile')){
            throw new RuntimeException("证据材料文件未输入或不正确");
        }
        UploadFile::convertBase64($this->evidenceFile);
    }
}