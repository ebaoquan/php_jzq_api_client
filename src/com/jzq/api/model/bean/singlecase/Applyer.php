<?php
/**
 * Created by PhpStorm.
 * User: yfx
 * Date: 2018/8/9
 * Time: 16:30
 */

namespace com\jzq\api\model\bean\singlecase;
use org\ebq\api\model\bean\UploadFile;
use RuntimeException;

/**
 * 申请人
 */
class Applyer{
    
    // 名称
    public $name;

    // 性质(0:个人、1:公司)
    public $nature;


    public $phone;


    public $email;

    // 证件地址
    public $address;

    // 联系地址
    public $contactAddress;

    // 身份证正面
    public $frontIdentityFile;


    // 身份证反面
    public $reverseIdentityFile;

    // 证件号
    public $identityNo;

    /**
     * 联系人
     */
    public $contacts;

    // 性别(0:男、1:女)
    public $gender;

    /**
     * 证件类型
     */
    public $idCardType;

    function validate(){
        $this->nature= self::trim($this->nature);
        $this->gender= self::trim($this->gender);
        $this->idCardType= self::trim($this->idCardType);
        if($this->frontIdentityFile==null||!is_a($this->frontIdentityFile, 'org\ebq\api\model\bean\UploadFile')){
            throw new RuntimeException("申请人身份证正面照文件未输入或不正确");
        }
        if($this->reverseIdentityFile==null||!is_a($this->reverseIdentityFile, 'org\ebq\api\model\bean\UploadFile')){
            throw new RuntimeException("申请人身份证反面照文件未输入或不正确");
        }
        UploadFile::convertBase64($this->frontIdentityFile);
        UploadFile::convertBase64($this->reverseIdentityFile);
    }

    /**
     * @param null $str
     * @return string
     */
    static function trim($str=null){
        if(is_null($str)){
            if(is_numeric($str)){
                return '0';
            }
            return '';
        }else{
            return trim($str.'');
        }
    }
}