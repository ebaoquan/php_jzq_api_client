<?php
/**
 * Created by PhpStorm.
 * User: yfx
 * Date: 2018/8/9
 * Time: 16:30
 */

namespace com\jzq\api\model\bean\singlecase;
use org\ebq\api\model\bean\UploadFile;
use RuntimeException;

/**
 * 代理人
 */
class Agent{

    
    // 代理人分类(0:律师代理、1:公民代理)
    public $agentType;

    // 代理人姓名
    public $name;

    // 身份证号
    public $identityNo;

    // 性别(0:男、1:女)
    public $sex;

    // 手机
    public $phone;

    // 邮箱
    public $email;

    // 证件地址
    public $address;

    // 联系地址
    public $contactAddress;

    // 身份证正面
    public $frontIdentityFile;

    // 身份证反面
    public $reverseIdentityFile;

    // 代理权限(0:一般代理、1:特殊代理)
    public $power;

    // 特殊权限项(格式：1,2,3)
    /**
     * 1、 代为提起仲裁请求
     * 2、 代为参加庭审、进行质证、辩论
     * 3、 代为和解、调解
     * 4、 代为主张、变更、放弃仲裁请求
     * 5、 代为签收法律文书
     * 6、 代为申请执行
     */
    public $powerDetail;

    // 特殊权限项(中文存储)
    /**
     * 1、 代为提起仲裁请求
     * 2、 代为参加庭审、进行质证、辩论
     * 3、 代为和解、调解
     * 4、 代为主张、变更、放弃仲裁请求
     * 5、 代为签收法律文书
     * 6、 代为申请执行
     */
    public $powerDetailZh;

    // 委托书
    public $wtsFile;

    /**
     * 验证数据
    */
    function validate(){
        $this->agentType= self::trim($this->agentType);
        $this->sex= self::trim($this->sex);
        $this->power= self::trim($this->power);
        if($this->wtsFile==null||!is_a($this->wtsFile, 'org\ebq\api\model\bean\UploadFile')){
            throw new RuntimeException("委托书文件未输入或不正确");
        }
        if($this->frontIdentityFile==null||!is_a($this->frontIdentityFile, 'org\ebq\api\model\bean\UploadFile')){
            throw new RuntimeException("代理人身份证正面照文件未输入或不正确");
        }
        if($this->reverseIdentityFile==null||!is_a($this->reverseIdentityFile, 'org\ebq\api\model\bean\UploadFile')){
            throw new RuntimeException("代理人身份证反面照文件未输入或不正确");
        }
        UploadFile::convertBase64($this->wtsFile);
        UploadFile::convertBase64($this->frontIdentityFile);
        UploadFile::convertBase64($this->reverseIdentityFile);
    }

    /**
     * @param null $str
     * @return string
     */
    static function trim($str=null){
        if(is_null($str)){
            if(is_numeric($str)){
                return '0';
            }
            return '';
        }else{
            return trim($str.'');
        }
    }
}
