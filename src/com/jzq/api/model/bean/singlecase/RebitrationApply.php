<?php
/**
 * Created by PhpStorm.
 * User: yfx
 * Date: 2018/8/9
 * Time: 16:30
 */

namespace com\jzq\api\model\bean\singlecase;
use org\ebq\api\model\bean\UploadFile;
use RuntimeException;

/**
 * 仲裁申请书
 */
class RebitrationApply{

    //请求标的
    public $disputFee;

    //请求事项
    public $claimContent;
    
    //事实及理由
    public $reason;
    
    //仲裁申请书名称
    public $applyFileName;
    
    //仲裁申请书
    public $applyFile;

    function validate(){
        if($this->applyFile==null||!is_a($this->applyFile, 'org\ebq\api\model\bean\UploadFile')){
            throw new RuntimeException("仲裁申请书文件未输入或不正确");
        }
        UploadFile::convertBase64($this->applyFile);
    }
}