<?php
/**
 * Created by PhpStorm.
 * User: yfx
 * Date: 2018/8/9
 * Time: 16:30
 */

namespace com\jzq\api\model\bean\singlecase;
use org\ebq\api\model\bean\UploadFile;
use RuntimeException;

/**
 * 合同信息
 */
class CaseContract{
    /**
     * APL编号
     */
    public $applyNo;

    /**
     * 合同文件
     */
    public $contractFile;

    
    function validate(){
        if($this->contractFile==null||!is_a($this->contractFile, 'org\ebq\api\model\bean\UploadFile')){
            throw new RuntimeException("合同文件未输入或不正确");
        }
        UploadFile::convertBase64($this->contractFile);
    }
}