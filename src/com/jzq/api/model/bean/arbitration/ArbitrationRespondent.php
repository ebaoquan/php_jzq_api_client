<?php
namespace com\jzq\api\model\bean\arbitration;
use com\jzq\api\model\menu\arbitration\ArbitrationIdentityType;
use com\jzq\api\model\menu\GenderType;
use org\ebq\api\model\RichServiceRequest;

/**
 * @author yfx 2018-08-20
 * 仲裁申请-被申请人
 */
class ArbitrationRespondent{
    /**被申请人姓名*/
    public $name;

    /**民族*/
    public $nation;

    /**
     * 性别 0：女 1：男
     * @see com.junziqian.api.common.GenderType
     */
    public $gender;

    /**身份证号码*/
    public $identityNo;

    /**送达手机号*/
    public $mobileNo;

    /**银行卡号 非必填*/
    public $bankCardNo;

    /**银行预留手机号码 非必填*/
    public $bankCardMobile;

    /**邮箱  非必填*/
    public $email;

    /**住址  非必填*/
    public $address;

    /**
     *  被申请人类型 0=自然人，1=企业
     *  @see com.junziqian.api.common.ArbitrationIdentityType
     */
    public $type;

    /**企业名称*/
    public $enterpriseName;

    /**营业执照*/
    public $businessLicence;

    /**法定代表人职务*/
    public $legalJob;

    function validate(){
        $this->type=RichServiceRequest::trim($this->type);
        $this->gender=RichServiceRequest::trim($this->gender);
        if(is_null($this->name)){
            throw new \RuntimeException("被申请人法人代表姓名不能为空");
        }else if(is_null($this->mobileNo)){
            throw new \RuntimeException("被申请人法人手机号不能为空");
        }
        if($this->type==ArbitrationIdentityType::$ENTERPRISE){
            if(is_null($this->enterpriseName)){
                throw new \RuntimeException("被申请人企业名称不能为空");
            }else if(is_null($this->businessLicence)){
                throw new \RuntimeException("被申请人统一社会信用代码不能为空");
            }else if(is_null($this->address)){
                throw new \RuntimeException("被申请人企业地址不能为空");
            }else if(is_null($this->legalJob)){
                throw new \RuntimeException("被申请人法定代表人职务不能为空");
            }else if(is_null($this->email)){
                throw new \RuntimeException("被申请人邮箱不能为空");
            }
        }else if($this->type==ArbitrationIdentityType::$NATURAL_MAN){
            if(is_null($this->nation)){
                throw new \RuntimeException("被申请人民族不能为空");
            }else if(is_null($this->identityNo)){
                throw new \RuntimeException("被申请人身份证号码不能为空");
            }
            if($this->gender!=GenderType::$MALE&&$this->gender!=GenderType::$FEMALE){
                throw new \RuntimeException("被申请人性别非法");
            }
        }else{
            throw new \RuntimeException("被申请人类型非法");
        }
        return true;
    }
}