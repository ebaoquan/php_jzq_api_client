<?php
namespace com\jzq\api\model\bean\arbitration;
use com\jzq\api\model\menu\BorrowDaysType;
use com\jzq\api\model\menu\RepaymentType;
use com\jzq\api\model\menu\YesOrNo;
use org\ebq\api\model\RichServiceRequest;

/**
 * @author yfx 2018-08-20
 * 仲裁申请-业务类型-借贷信息
 */
class ArbitrationLoanInfo{
    /**
     * 合同名称
     */
    public $contractName;

    /**
     * 签订日期
     */
    public $signDate;

    /**
     * 出借日期
     */
    public $borrowDate;

    /**
     * 出借金额（标的额）
     */
    public $borrowMoney;

    /**
     *  还款方式 （0=等额本息、1=一次性偿还）
     *  @see com.junziqian.api.common.RepaymentType
     */
    public $repaymentType;

    /**
     * 还款限期
     */
    public $borrowDays;

    /**
     * 还款期限类型  0=天，1=期
     * @see  com.junziqian.api.common.BorrowDaysType
     */
    public $borrowDaysType;

    /**
     * 每期还款金额  borrowDaysType=1，则必填
     */
    public $repayment;

    /**
     * 每期还款金额月利率，borrowDaysType=1，则必填，1.6% 填1.6
     */
    public $monthMoneyRate;

    /**
     * 借款利率(年，百分比)，1.6% 填1.6
     */
    public $moneyRate;

    /**
     * 应还利息
     */
    public $interest;

    /**
     * 逾期利率
     */
    public $overdueMoney;

    /**
     *  末次还款日期  非必填
     */
    public $lastestRepaymentDate;



    /**
     * 已还金额 lastestRepaymentDate 填了，则必填
     */
    public $repayMoney;

    /**
     *  应还款日期
     */
    public $repaymentDate;

    /**
     * 居间服务费   非必填
     */
    public $serviceFee;

    /**
     * 已还居间服务费 若serviceFee 填了，则必填
     */
    public $paidServiceFee;

    /**
     * 违约金 非必填
     */
    public $penalty;

    /**
     * 违约金利率（日利率）， 若penalty 填了，则必填
     */
    public $penaltyRate;

    /**
     * 罚息 非必填
     */
    public $fine;

    /**
     * 罚息利率（日利率）， 若fine 填了，则必填
     */
    public $fineRate;
    /**
     *  是否有抵押物 1=有，2=否  非必填
     */
    public $pawn;

    /**
     * 抵押物是否变卖，若pawn 填了，则必填 1=是， 2=否  非必填
     */
    public $pawnSold;

    /**
     *  变卖金额  若pawn填了，则必填
     */
    public $pawnSoldMoney;

    /**
     * 律师服务费 非必填
     */
    public $layerServiceMoney;

    function validate(){
        $this->repaymentType=RichServiceRequest::trim($this->repaymentType);
        $this->borrowDaysType=RichServiceRequest::trim($this->borrowDaysType);
        if(is_null($this->contractName)){
            throw new \RuntimeException("合同名称不能为空");
        }
        if(!is_numeric($this->signDate)){
            throw new \RuntimeException("签订日期不能为空或格式不正确");
        }
        if(!is_numeric($this->borrowDate)){
            throw new \RuntimeException("出借日期不能为空或格式不正确");
        }
        if(!is_numeric($this->borrowMoney)){
            throw new \RuntimeException("出借金额不能为空或格式不正确");
        }
        if($this->repaymentType!=RepaymentType::$DEBX&&$this->repaymentType!=RepaymentType::$DISPOSABLE){
            throw new \RuntimeException("还款方式格式不正确");
        }
        if(!is_numeric($this->borrowDays)){
            throw new \RuntimeException("还款期限不能为空");
        }
        if($this->borrowDaysType!=BorrowDaysType::$DAY&&$this->borrowDaysType!=BorrowDaysType::$TERMS){
            throw new \RuntimeException("还款期限类型不能为空");
        }
        if($this->borrowDaysType==BorrowDaysType::$TERMS){
            if(!is_numeric($this->repayment)){
                throw new \RuntimeException("每期还款金额不能为空");
            }
            if(!is_numeric($this->monthMoneyRate)){
                throw new \RuntimeException("每期还款金额月利率不能为空");
            }
        }else{
            $this->repayment=null;
            $this->monthMoneyRate=null;
        }

        if(!is_numeric($this->moneyRate)){
            throw new \RuntimeException("借款利率不能为空");
        }
        if(!is_numeric($this->interest)){
            throw new \RuntimeException("应还利息不能为空");
        }
        if(!is_numeric($this->overdueMoney)){
            throw new \RuntimeException("逾期利率不能为空");
        }
        //填了末次还款日期，必须填已还金额
        if ($this->lastestRepaymentDate != null) {
            if(!is_numeric($this->repayMoney)){
                throw new \RuntimeException("已还金额不能为空");
            }
        } else {
            $this->repayMoney = null;
        }
        if(!is_numeric($this->repaymentDate)){
            throw new \RuntimeException("应还款日期不能为空");
        }
        //填了居间服务费，则已还居间服务费必填
        if ($this->serviceFee != null) {
            if(!is_numeric($this->paidServiceFee)){
                throw new \RuntimeException("已还居间服务费不能为空");
            }
        } else {
            $this->paidServiceFee = null;
        }

        if ($this->penalty != null) {
            if(!is_numeric($this->penaltyRate)){
                throw new \RuntimeException("违约金利率（日利率）不能为空");
            }
        } else {
            $this->penaltyRate = null;
        }

        if ($this->fine != null) {
            if(!is_numeric($this->fineRate)){
                throw new \RuntimeException("罚息利率（日利率）不能为空");
            }
        } else {
            $this->fineRate = null;
        }

        if ($this->pawn != null) {
            if($this->pawn!=YesOrNo::$NO&&$this->pawn!=YesOrNo::$YES){
                throw new \RuntimeException("是否有抵押物值非法");
            }
            if ($this->pawn== YesOrNo::$YES) {
                if(!is_numeric($this->pawnSold)){
                    throw new \RuntimeException("抵押物是否变卖不能为空");
                }
                if(!is_numeric($this->pawnSoldMoney)){
                    throw new \RuntimeException("变卖金额不能为空");
                }
                //若抵押物已经变卖
                if ($this->pawnSold == YesOrNo::$YES) {
                    if(!is_numeric($this->pawnSoldMoney)){
                        throw new \RuntimeException("变卖金额不能为空");
                    }
                } else {
                    $this->pawnSoldMoney = null;
                }
            } else {
                $this->pawnSold = null;
                $this->pawnSoldMoney = null;
            }
        } else {
            $this->pawnSold = null;
            $this->pawnSoldMoney = null;
        }
        return true;
    }
}