<?php
namespace com\jzq\api\model\bean\arbitration;

use org\ebq\api\model\bean\UploadFile;
use org\ebq\api\tool\RopUtils;

/**
 * @author yfx 2018-08-20
 * 仲裁申请-证据清单
 */
class ArbitrationEvidence{
    /**凭证文件最大长度:2MB*/
    static $MAX_VOUCHER_FILE_M=2;
    /**凭证最多上传10个*/
    static $MAX_VOUCHER_SIZE = 10;
    /**身份证/营业执照文件最大长度:5MB*/
    static $MAX_IDENTIFICATION_FILELENGTH_M=5;
    /**备注长度限制*/
    static $REMARK_LIMIT=250;

    /**
     *  申请人身份证/营业执照扫描件
     */
    public $claimantIdentityImg;

    /**
     * 被申请人身份证/营业执照扫描件
     */
    public $respondentIdentityImg;

    /**
     *  申请人法人身份证扫描件
     */
    public $claimantLegalIdNoImg;

    /**
     * 被申请人法人身份证扫描件
     */
    public $respondentLegalIdNoImg;

    /**
     * 转账凭证
     */
    public $transferVoucherList;

    /**
     * 辅助凭证
     */
    public $assistantVoucherList;

    /**验证*/
    function validate(){
        if($this->claimantIdentityImg==null||!is_a($this->claimantIdentityImg, 'org\ebq\api\model\bean\UploadFile')){
            throw new \RuntimeException("申请人身份证/营业执照扫描件不能为空");
        }else{
            UploadFile::convertBase64($this->claimantIdentityImg);
        }
        if($this->respondentIdentityImg==null||!is_a($this->respondentIdentityImg, 'org\ebq\api\model\bean\UploadFile')){
            throw new \RuntimeException("被申请人身份证/营业执照扫描件不能为空");
        }else{
            UploadFile::convertBase64($this->respondentIdentityImg);
        }
        if((!is_array($this->transferVoucherList))||count($this->transferVoucherList)==0){
            throw new \RuntimeException("转账凭证不能为空");
        }else{
            if (count($this->transferVoucherList) > self::$MAX_VOUCHER_SIZE) {
                throw new \RuntimeException("转账凭证个数不能超过".self::$MAX_VOUCHER_SIZE);
            }
        }
        if($this->claimantLegalIdNoImg!=null){
            if(!is_a($this->claimantLegalIdNoImg, 'org\ebq\api\model\bean\UploadFile')){
                throw new \RuntimeException("申请人法人照扫描件不能为空");
            }
            UploadFile::convertBase64($this->claimantLegalIdNoImg);
        }
        if($this->respondentLegalIdNoImg!=null){
            if(!is_a($this->respondentLegalIdNoImg, 'org\ebq\api\model\bean\UploadFile')){
                throw new \RuntimeException("被申请人法人照扫描件不能为空");
            }
            UploadFile::convertBase64($this->respondentLegalIdNoImg);
        }
        foreach ($this->transferVoucherList as $val){
            $this->checkVoucherRemark($val,"转账凭证");
        }
        if((is_array($this->assistantVoucherList))&&count($this->assistantVoucherList)>0){
            if (count($this->assistantVoucherList) > self::$MAX_VOUCHER_SIZE) {
                throw new \RuntimeException("辅助凭证个数不能超过".self::$MAX_VOUCHER_SIZE);
            }
            foreach ($this->assistantVoucherList as $val){
                $this->checkVoucherRemark($val,"辅助凭证");
            }
        }
        return true;
    }

    /**
     * @param $voucherRemark VoucherRemark
     * @param $str string
     */
    function checkVoucherRemark($voucherRemark,$str){
        if(!is_a($voucherRemark, 'com\jzq\api\model\bean\arbitration\VoucherRemark')){
            throw new \RuntimeException($str."类型不正确");
        }
        if(is_null($voucherRemark->uploadFile)||!is_a($voucherRemark->uploadFile,'org\ebq\api\model\bean\UploadFile')){
            throw new \RuntimeException( $str."中，凭证文件不能为空");
        }
        if(is_null($voucherRemark->remark)){
            throw new \RuntimeException( $str."中，备注不能为空");
        }
        //转换文本2进制为base64SafeUrl(直接用json_encode是不能转换的)
        UploadFile::convertBase64($voucherRemark->uploadFile);
    }
}