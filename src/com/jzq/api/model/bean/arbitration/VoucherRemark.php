<?php
namespace com\jzq\api\model\bean\arbitration;
use org\ebq\api\model\bean\UploadFile;

/**
 * @author yfx 2018-08-20
 * 仲裁申请-凭证信息
 */
class VoucherRemark{
    /**凭证文件*/
    public $uploadFile;
    /**备注*/
    public $remark;

    /**
     * VoucherRemark constructor.
     * @param $uploadFile UploadFile
     * @param $remark
     */
    function __construct($uploadFile,$remark){
        $this->uploadFile=$uploadFile;
        $this->remark=$remark;
    }
}