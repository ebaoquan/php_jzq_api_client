<?php
/**
 * Created by PhpStorm.
 * User: huhu
 * Date: 2018/9/6
 * Time: 20:40
 */

namespace com\jzq\api\model\tmpl;
use org\ebq\api\model\RichServiceRequest;
use org\ebq\api\tool\RopUtils;
use RuntimeException;

class TmplPreRequest extends RichServiceRequest{
    static $v="1.0";
    static $method="tmpl.pre";
    /**
     * 合同模板编号
     * @var string 非空，最长100个字符
     */
    public $templateNo;
    /**
     * @var array 合同内容填充参数
     */
    public $contractParams;

    function validate(){
        if(!is_string($this->templateNo)||strlen($this->templateNo)==0||strlen($this->templateNo)>100){
            throw new RuntimeException("templateNo is null or size gt 100");
        }
        if($this->contractParams!=null&&!is_array($this->contractParams)){
            throw new RuntimeException("contractParams isn't a array");
        }
        $this->contractParams=RopUtils::json_encode($this->contractParams);
        return parent::validate();
    }

}