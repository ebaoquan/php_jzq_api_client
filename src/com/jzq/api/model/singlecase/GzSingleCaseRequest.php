<?php
/**
 * Created by vs code.
 * User: yfx
 * Date: 2019/01/17
 * Time: 16:30
 */
namespace com\jzq\api\model\singlecase;

use org\ebq\api\model\RichServiceRequest;
use org\ebq\api\tool\RopUtils;
use RuntimeException;

class GzSingleCaseRequest  extends RichServiceRequest{
    static $v="1.0";
    static $method="gz.single.case.apply";
    /**
     * 合同信息
     */
    public $caseContracts;

    /**
     * 申请人
     */
    public $applyer;

    /**
     * 被申请人
     */
    public $respondent;

    /**
     * 仲裁申请书
     */
    public $rebitrationApply;

    /**
     * 证据清单
     */
    public $evidences;

    /*代理人信息*/
    public $agent;

    /**
     * 校验方法
     */
    function validate(){
        if($this->applyer==null||!is_a($this->applyer, 'com\jzq\api\model\bean\singlecase\Applyer')){
            throw new RuntimeException("申请人信息不能为空");
        }
        if($this->rebitrationApply==null||!is_a($this->rebitrationApply, 'com\jzq\api\model\bean\singlecase\RebitrationApply')){
            throw new RuntimeException("仲裁申请书不能为空");
        }
        if($this->respondent==null||!is_a($this->respondent, 'com\jzq\api\model\bean\singlecase\Respondent')){
            throw new RuntimeException("申请人信息不能为空");
        }
        if($this->agent==null||!is_a($this->agent, 'com\jzq\api\model\bean\singlecase\Agent')){
            throw new RuntimeException("申请人信息不能为空");
        }
        $this->applyer->validate();
        $this->rebitrationApply->validate();
        $this->respondent->validate();
        $this->agent->validate();

        $this->applyer=RopUtils::json_encode($this->applyer);
        $this->rebitrationApply=RopUtils::json_encode($this->rebitrationApply);
        $this->respondent=RopUtils::json_encode($this->respondent);
        $this->agent=RopUtils::json_encode($this->agent);

        if($this->caseContracts==null||!is_array($this->caseContracts)){
            throw new RuntimeException("合同信息不能为空");
        }
        if($this->evidences==null||!is_array($this->evidences)){
            throw new RuntimeException("证据材料不能为空");
        }
        foreach ($this->caseContracts as $val){
            if(!is_a($val,'com\jzq\api\model\bean\singlecase\CaseContract')){
                throw new RuntimeException("caseContract is not a CaseContract value");
            }
            $val->validate();
        }
        foreach ($this->evidences as $val){
            if(!is_a($val,'com\jzq\api\model\bean\singlecase\Evidence')){
                throw new RuntimeException("evidence is not a Evidence value");
            }
            $val->validate();
        }
        $this->caseContracts=RopUtils::json_encode($this->caseContracts);
        $this->evidences=RopUtils::json_encode($this->evidences);
        return parent::validate();
    }
}