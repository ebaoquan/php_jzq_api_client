<?php
/**
 * Created by PhpStorm.
 * User: yfx
 * Date: 2018/8/9
 * Time: 16:30
 */

namespace com\jzq\api\model\sms;
use org\ebq\api\model\RichServiceRequest;
use RuntimeException;

/**
 * 验证短信验证码
 * @edit yfx 2018-08-01
 */
class SmsCodeVerifyRequest extends RichServiceRequest{
    static $v="1.0";
    static $method="sms.code.verify";

    /**业务流水号*/
    public $bizNo;
    /**收到的短信验证码*/
    public $code;
    /**是否验证成功后删除*/
    public $exipreAfterSuccess;
    /**是否验证成功后保全，如果使用保全，则exipreAfterSuccess会被强制为true*/
    public $presFlag;

    function validate(){
        $this->bizNo=self::trim($this->bizNo);
        if($this->bizNo==''){
            throw new RuntimeException("bizNo is null");
        }
        $this->code=self::trim($this->code);
        if($this->code==''){
            throw new RuntimeException("code is null");
        }
        $this->exipreAfterSuccess=self::trim($this->exipreAfterSuccess);
        $this->presFlag=self::trim($this->presFlag);
        return true;
    }
}