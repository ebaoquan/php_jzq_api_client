<?php
/**
 * Created by PhpStorm.
 * User: yfx
 * Date: 2018/8/9
 * Time: 16:30
 */

namespace com\jzq\api\model\sms;
use org\ebq\api\model\RichServiceRequest;
use org\ebq\api\tool\RopUtils;
use RuntimeException;
/**
 * 发送短信验证码
 * @edit yfx 2018-08-01
 */
class SmsCodeSendRequest  extends RichServiceRequest{
    static $v="1.0";
    static $method="sms.code.send";

    /**验证模板编号*/
    public $templateNo;
    /**手机号*/
    public $mobile;
    /**失效时长,2分钟到1小时不等,默认30分钟,单位毫秒;注失效后再发送将发送新的验证码;对于超2分钟再发送在30分钟还是相同的验证码这种需客户端实现*/
    public $exipreLen;
    /**模版参数*/
    public $params;

    function validate(){
        $this->templateNo=self::trim($this->templateNo);
        if($this->templateNo==''){
            throw new RuntimeException("templateNo is null");
        }
        $this->mobile=self::trim($this->mobile);
        if($this->mobile==''){
            throw new RuntimeException("mobile is null");
        }
        if($this->params!=null&&!is_array($this->params)){
            throw new RuntimeException("params isn't a array");
        }
        if($this->params!=null){
            $this->params=RopUtils::json_encode($this->params);
        }
        return true;
    }
}