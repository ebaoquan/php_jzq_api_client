<?php
namespace com\jzq\api\model\arbitration;

use com\jzq\api\model\menu\arbitration\BusinessTransferType;
use org\ebq\api\model\RichServiceRequest;
use org\ebq\api\tool\RopUtils;

/**
 * @author yfx 2018-08-20
 * 仲裁申请
 */
class ArbitrationApplyRequest extends RichServiceRequest{

    static $v="1.0";
    static $method="arbitration.apply";
    /**签约编号*/
    public $applyNo;
    /**申请人:json*/
    public $claimant;
    /**被申请人:json*/
    public $respondent;
    /**业务类型:BusinessTransferType*/
    public $businessType;
    /**仲裁业务*/
    public $business;
    /**证据清单*/
    public $evidence;
    /**
     * 代理类型 0:一般代理 1：特殊代理 默认0
     */
    public $agentType;

    function validate(){
        $this->applyNo=self::trim($this->applyNo);
        if($this->applyNo==''){
            throw new \RuntimeException("签约编号不能为空");
        }
        if($this->businessType!=BusinessTransferType::$LOAN){
            throw new \RuntimeException("仲裁业务类型不正确或为空");
        }
        if($this->business==null){
            throw new \RuntimeException("仲裁业务信息不能为空");
        }
        $this->business->validate();

        if($this->claimant==null){
            throw new \RuntimeException("申请人信息不能为空");
        }
        $this->claimant->validate();

        if($this->respondent==null){
            throw new \RuntimeException("被申请人信息不能为空");
        }
        $this->respondent->validate();

        if($this->evidence==null){
            throw new \RuntimeException("证据清单信息不能为空");
        }
        $this->evidence->validate();

        $this->business=RopUtils::json_encode($this->business);
        $this->claimant=RopUtils::json_encode($this->claimant);
        $this->respondent=RopUtils::json_encode($this->respondent);
        $this->evidence=RopUtils::json_encode($this->evidence);
        return parent::validate();
    }

    /**
     * 不签名的filed
     */
    function getIgnoreSign(){
        $ignoreSign=array('evidence');
        return $ignoreSign;
    }

    function __toString(){
        return json_encode($this, JSON_UNESCAPED_UNICODE);
    }
}