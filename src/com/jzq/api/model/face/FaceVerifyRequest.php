<?php
namespace com\jzq\api\model\face;

use org\ebq\api\model\RichServiceRequest;
/**
 * 人脸图片识别
 * User: yfx
 * Date: 2017/12/27
 * Time: 9:57
 */
class FaceVerifyRequest extends RichServiceRequest{
    static $v="1.0";
    static $method="face.verify";
    /**
     * @var string 证件ID
     */
    public $identityCard;

    /**
     * @var string 证件名称
     */
    public $fullName;
    /**
     * @var string 待校验的文件
     */
    public $file;

    /**
     * 校验规则
     * @return bool
     */
    function validate(){
        assert(!is_null($this->identityCard),"证件ID不能为空");
        assert(!is_null($this->fullName),"证件名称不能为空");
        assert(is_a($this->file,'org\ebq\api\model\bean\UploadFile'),"待校验的图片不能为空，或不是一个UploadFile对象");
        return parent::validate();
    }

    /**
     * 不签名的filed
     */
    function getIgnoreSign(){
        $ignoreSign=array('file');
        $parr=parent::getIgnoreSign();
        if(is_array($parr)){
            $ignoreSign=array_merge($ignoreSign,$parr);
        }
        return $ignoreSign;
    }
}