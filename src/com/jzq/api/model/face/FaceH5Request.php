<?php
/**
 * Created by PhpStorm.
 * User: huhu
 * Date: 2018/9/6
 * Time: 10:30
 */
namespace com\jzq\api\model\face;
use com\jzq\api\model\menu\face\FaceH5StartFromType;
use org\ebq\api\model\RichServiceRequest;
use RuntimeException;
/**
 * 人脸认证
 * User: yfx
 * Date: 2018/09/06
 * Time: 9:57
 */
class FaceH5Request extends RichServiceRequest{
    static $v="1.0";
    static $method="start.h5.face";

    /**企业用户id*/
    public $userId;
    /**订单号*/
    public $orderNo;
    /**姓名*/
    public $name;
    /**身份证号*/
    public $idNo;
    /**启动方式1=browser,2=app*/
    public $startFrom;
    /**回调地址*/
    public $backUrl;

    function validate(){
        $this->userId=self::trim($this->userId);
        $this->orderNo=self::trim($this->orderNo);
        $this->name=self::trim($this->name);
        $this->idNo=self::trim($this->idNo);
        $this->startFrom=self::trim($this->startFrom);
        if($this->userId!=''){
            if(strlen($this->userId)>32){
                throw new RuntimeException("用户id长度不能超过32个字符");
            }
        }
        if($this->orderNo==''){
            throw new RuntimeException("订单号不能为空");
        }
        if(strlen($this->orderNo)>32){
            throw new RuntimeException("订单号长度不能超过32个字符");
        }
        if($this->name==''){
            throw new RuntimeException("用户姓名不能为空");
        }
        if($this->idNo==''){
            throw new RuntimeException("用户身份证号不能为空");
        }
        if($this->startFrom!=FaceH5StartFromType::$BROWSER&&$this->startFrom!=FaceH5StartFromType::$APP){
            throw new RuntimeException("启动方式值输入不正确");
        }
        return parent::validate();
    }
}