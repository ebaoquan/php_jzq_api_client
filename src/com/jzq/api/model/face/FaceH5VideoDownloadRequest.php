<?php
namespace com\jzq\api\model\face;
use com\jzq\api\model\menu\face\FaceH5StartFromType;
use org\ebq\api\model\RichServiceRequest;
use RuntimeException;

/**
 * 人脸认证视频下载
 * User: yfx
 * Date: 2018/09/06
 * Time: 9:57
 */
class FaceH5VideoDownloadRequest extends RichServiceRequest{
    static $v="1.0";
    static $method="h5.face.video.download";

    /**订单号*/
    public $orderNo;

    function validate(){
        $this->orderNo=self::trim($this->orderNo);
        if($this->orderNo==''){
            throw new RuntimeException("订单号不能为空");
        }
        if(strlen($this->orderNo)>32){
            throw new RuntimeException("订单号长度不能超过32个字符");
        }
        return parent::validate();
    }
}